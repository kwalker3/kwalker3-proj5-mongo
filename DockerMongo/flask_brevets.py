"""
=== Project 5 Version ===
Edited by Kayla Walker
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

from pymongo import MongoClient

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetdb

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    brevet_dist = request.args.get('brevet_dist', 200, type=int)

    begin_date = request.args.get('begin_date', "2017-01-01", type=str)
    begin_time = request.args.get('begin_time', "00:00", type=str)
    # now convert begin date and begin time into arrow, then into string
    begin_arrow = arrow.get(begin_date + ' ' + begin_time, 'YYYY-MM-DD HH:mm')
    app.logger.debug("begin_arrow={}".format(begin_arrow))

    open_time = acp_times.open_time(km, brevet_dist, begin_arrow.isoformat())
    close_time = acp_times.close_time(km, brevet_dist, begin_arrow.isoformat())
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)

@app.route('/_display')
def _display():
    _items = db.brevetdb.find()
    items = [item for item in _items]

    return render_template('display_page.html', items=items)

@app.route('/_submit')
def _submit():

    db.brevetdb.drop()

    km_list = request.args.getlist('km[]')
    open_list = request.args.getlist('open[]')
    close_list = request.args.getlist('close[]')

    print(km_list)
    print(open_list)
    print(close_list)

    for i in range(len(km_list)):
        item_doc = {
            'km': km_list[i],
            'open': open_list[i],
            'close': close_list[i]
        }
        db.brevetdb.insert_one(item_doc)

    return redirect(url_for('index'))


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
